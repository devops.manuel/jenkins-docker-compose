# jenkins-docker-compose

Crear el usuario jenkins y agregar su password

```bash
  sudo useradd -m jenkins

  sudo passwd jenkins
```

correr el contenedor de jenkins
```bash
  docker-compose up -d
```

# Ingresar la ruta de Jenkins

***http://localhost:8080*** 

***http://192.168.0.10:8080/***

obtenemos el password

```bash
  docker logs jenkins | less
```

ejemplo de la salida por consola
```
*************************************************************
*************************************************************
*************************************************************

Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:

6662dbfac8044eb29c08ea71ee083be2

This may also be found at: /var/jenkins_home/secrets/initialAdminPassword

*************************************************************
*************************************************************
*************************************************************
```

Presionamos la opción de ***Intall suggested plugins***

esperamos a que los plugins se instalen correctamente 